﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoCrud.Models
{
    public class Books
    {
        public enum BookState
        {
			[BsonRepresentation(BsonType.String)] New,
			[BsonRepresentation(BsonType.String)] WasInUsage,           
        }


        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string id { get; set; }
        public string name { get; set; }
        public int price { get; set; }
        public string category { get; set; }
        public string author { get; set; }
        public BookState bookState { get; set; }
    }
}
