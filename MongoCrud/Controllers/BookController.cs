﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoCrud.Models;
using MongoCrud.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoCrud.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class BookController : ControllerBase
	{
		private readonly IBookService _bookService;
		public BookController(IBookService bookService)
		{
			_bookService = bookService;
		}

		[HttpGet]
		public async Task<IActionResult> GetAll()
		{
			return Ok(await _bookService.GetAllAsync());
		}

		[HttpGet("{id}")]
		public async Task<IActionResult> Get(string id)
		{
			var book = await _bookService.GetByIdAsync(id);
			if (book == null)
			{
				return NotFound();
			}
			return Ok(book);
		}

		[HttpPost]
		public async Task<IActionResult> Create(Books book)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest();
			}
			await _bookService.CreateAsync(book);
			return Ok(book.id);
		}

		[HttpPut]
		public async Task<IActionResult> Update(string id, Books booksData)
		{
			var book = await _bookService.GetByIdAsync(id);
			if (book == null)
			{
				return NotFound();
			}
			await _bookService.UpdateAsync(id, booksData);
			return NoContent();
		}

		[HttpDelete]
		public async Task<IActionResult> Delete(string id)
		{
			var book = await _bookService.GetByIdAsync(id);
			if (book == null)
			{
				return NotFound();
			}
			await _bookService.DeleteAsync(book.id);
			return NoContent();
		}
	}
}
